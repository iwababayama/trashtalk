<?php
    session_start();

    //変数読み込み
    include 'function.php';

    //セッション変数受け渡し
    //ログインフラグ：ＯＮ
    if (isset($_SESSION["user_name"])) {
        $user_name = $_SESSION["user_name"];
        $login_flg = 1;
    }

    //データベース読み込み、新規ＰＤＯ作製
    $db = new PDO("mysql:host=localhost;dbname=trashtalk", "root", "");

    // 個別のデータを読み込む(users, stoke, stoker, posts)
    $sql_user = "SELECT * FROM users WHERE user_name = '${user_name}'";
    $result_user = $db -> query($sql_user);
    $rows_user = $result_user -> fetchall(PDO::FETCH_ASSOC);

    $sql_stoke = "SELECT * FROM stoke_$user_name";
    $result_stoke = $db -> query($sql_stoke);
    $rows_stoke = $result_stoke -> fetchall(PDO::FETCH_ASSOC);

    $sql_stoker = "SELECT * FROM stoker_$user_name";
    $result_stoker = $db -> query($sql_stoker);
    $rows_stoker = $result_stoker -> fetchall(PDO::FETCH_ASSOC);

    $sql_posts = "SELECT * FROM posts WHERE user_name = '${user_name}'";
    $result_posts = $db -> query($sql_posts);
    $rows_posts = $result_posts -> fetchall(PDO::FETCH_ASSOC);

    //投稿時間で並べ替え
    $sort = array();
    foreach ($rows_posts as $key => $value) {
        $sort[$key] = $value['post_date'];
    }

    array_multisort($sort, SORT_DESC, $rows_posts);

    //以下、全コメントの取得
    $sql_comment = "SELECT * FROM COMMENTS";
    $result_comment = $db -> query($sql_comment);
    $rows_comment = $result_comment -> fetchall(PDO::FETCH_ASSOC);
