<?php

    //データベース読み込み、新規ＰＤＯ作成
    $db = new PDO("mysql:host=localhost;dbname=trashtalk", "root", "");

    //$user_name_getに値があるときは、$user_name_timelineに$user_name_getを代入(timeline.phpを使いまわすため)
    // if (isset($_GET["user_name_get"])){
    //   $user_name = $_GET["user_name_get"];
    // }

    //postsテーブルのuse_nameと、stoke_$user_nameテーブルのstokeが一致するレコードを読み出す
    $sql = "SELECT * FROM posts JOIN stoke_" . $user_name . " ON posts.user_name = stoke_" . $user_name . ".stoke";
    $result = $db -> query($sql);
    $rows = $result -> fetchall(PDO::FETCH_ASSOC);

    //投稿時間で並べ替え
    $sort = array();
    foreach ($rows as $key => $value) {
        $sort[$key] = $value['post_date'];
    }
    array_multisort($sort, SORT_DESC, $rows);

    //以下、全コメントの取得
    $sql_comment = "SELECT * FROM COMMENTS";
    $result_comment = $db -> query($sql_comment);
    $rows_comment = $result_comment -> fetchall(PDO::FETCH_ASSOC);
