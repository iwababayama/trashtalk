<?php
    //セッションスタート
    session_start();

    //変数読み込み
    include 'function.php';

    //セッション変数受け渡し
    //ログインフラグ：ＯＮ
    if (isset($_SESSION["user_name"])) {
        $user_name = $_SESSION["user_name"];
        $login_flg = 1;
    }

    //データベース読み込み、新規ＰＤＯ作成
    $db = new PDO("mysql:host=localhost;dbname=trashtalk", "root", "");

    //postsテーブルのuse_nameと、stoker_$user_nameテーブルのstokerが一致するレコードを読み出す
    $sql = "SELECT * FROM users JOIN stoker_" . $user_name . " ON users.user_name = stoker_" . $user_name . ".stoker";
    $result = $db -> query($sql);
    $rows = $result -> fetchall(PDO::FETCH_ASSOC);
