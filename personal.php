<?php
/**
 * Created by PhpStorm.
 * User: yamamototatsuya
 * Date: 2017/08/13
 * Time: 10:33
 */

//model読み込み
include 'personal_model.php';

?>

<?php include "header.php"; ?>
    <article class="personal">
        <div class="contents_personal">
            <img src="<?= $rows_user[0]['post_image']; ?>" width="150px" height="150px" alt="" class="img-circle">
            <div class="username"><h2><?= $user_name?></h2>
            <ul>
                <li>投稿数 <?= count($rows_posts); ?></li>
                <li>ストーク <a href="stoke.php"><?= count($rows_stoke); ?></a></li>
                <li>ストーカー <a href="stoker.php"><?= count($rows_stoker); ?></a></li>
            </ul>
            </div>
        </div>
        <?php foreach ($rows_posts as $row_posts) :?>
        <div class="container">
            <div class="personal_photo">
                <div class="row">
                   <div class="col-lg-4">
                       <img src="<?='./images/' . $row_posts['post_image'] ?>" alt="" width="300px">
                       <p><?=$row_posts['post_text'] ?></p>
                       <p><a href="personal_someone.php?user_name_get=<?= $row_posts['user_name']?>"><?=$row_posts['user_name'] ?></a></p>
                       <p><?=$row_posts['post_date'] ?></p>
                   </div>
                </div>
            </div>
            <?php foreach ($rows_comment as $rows) :?>
                <?php if ($row_posts['post_id'] === $rows['post_id']) :?>
                    <?= $rows['comment']; ?>
                    <?= $rows['user_name']; ?>
                    <?= "</br>"; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <?php endforeach; ?>
    </article>
<?php include "footer.php"; ?>
